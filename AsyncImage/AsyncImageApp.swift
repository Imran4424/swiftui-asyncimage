//
//  AsyncImageApp.swift
//  AsyncImage
//
//  Created by Shah Md Imran Hossain on 29/1/22.
//

import SwiftUI

@main
struct AsyncImageApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
